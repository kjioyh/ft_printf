/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   percent_x.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/15 19:25:44 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/27 19:37:34 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	vd_hexa_width(t_f *fl, char *str, int *arr)
{
	int width;

	width = fl->width - max_of_two(arr[1], fl->precision);
	if (fl->hash == 1 && arr[3] != 2)
		width -= 2;
	if (width > 0)
	{
		if (fl->minus == 0)
		{
			while (width != 0)
			{
				if (fl->zero == 1 && fl->precision == -1)
					str[arr[4]] = '0';
				else
					str[arr[4]] = ' ';
				arr[4] += 1;
				width--;
			}
		}
		else
		{
			arr[2] = width;
		}
	}
}

void	vd_hexa_hash(t_f *fl, int *arr, char *str, char letter)
{
	if (arr[3] != 2)
	{
		if (letter == 'x')
		{
			if (fl->hash == 1)
			{
				ft_strcpy(&str[arr[4]], "0x");
				arr[4] += 2;
				arr[3] = 1;
			}
		}
		else
		{
			if (fl->hash == 1)
			{
				ft_strcpy(&str[arr[4]], "0X");
				arr[4] += 2;
				arr[3] = 1;
			}
		}
	}
}

char	*vd_hexa_convert(char letter, uintmax_t hex)
{
	char *ptr;

	if (letter == 'X')
	{
		ptr = vd_huo_itoa(hex, 16, 1);
	}
	else
	{
		ptr = vd_huo_itoa(hex, 16, 0);
	}
	return (ptr);
}

void	vd_unsign_width(t_f *fl, char *str, int *arr)
{
	int width;

	width = fl->width - max_of_two(arr[1], fl->precision);
	if (width > 0)
	{
		if (fl->minus == 0)
		{
			while (width != 0)
			{
				if (fl->zero == 1 && fl->precision == -1)
					str[arr[4]] = '0';
				else
					str[arr[4]] = ' ';
				arr[4] += 1;
				width--;
			}
		}
		else
		{
			arr[2] = width;
		}
	}
}
