/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/16 13:29:03 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/28 18:34:06 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			p_func1(const char *format, int *i, va_list ap, t_f *fl)
{
	int		err;

	if (format[*i] == 'd' || format[*i] == 'i' || format[*i] == 'D')
		err = ft_id(va_arg(ap, void *), fl, format[*i]);
	else if (format[*i] == 'c' || format[*i] == 'C')
		err = ft_c(va_arg(ap, void *), fl, format[*i]);
	else if (format[*i] == 's')
		err = ft_string(va_arg(ap, char *), fl);
	else if (format[*i] == 'o' || format[*i] == 'O')
		err = ft_o(va_arg(ap, void *), fl, format[*i]);
	else if (format[*i] == 'x' || format[*i] == 'X')
		err = ft_x(va_arg(ap, void *), fl, format[*i]);
	else if (format[*i] == 'u' || format[*i] == 'U')
		err = ft_u(va_arg(ap, void *), fl, format[*i]);
	else if (format[*i] == 'p')
		err = ft_memory(va_arg(ap, void *), fl);
	else if (format[*i] == '\0')
	{
		err = 0;
		return (err);
	}
	else
		err = p_func2(format, i, ap, fl);
	*i += 1;
	return (err);
}

int			p_func2(const char *format, int *i, va_list ap, t_f *fl)
{
	int		err;

	if (format[*i] == 'S')
		err = ft_unistring(va_arg(ap, wchar_t *), fl);
	else
		err = ft_char(format[*i], fl);
	return (err);
}

int			ft_printf(const char *format, ...)
{
	int		i;
	va_list	ap;
	t_f		fl;

	g_count = 0;
	i = 0;
	va_start(ap, format);
	while (format[i] != '\0')
		if (format[i] == '%')
		{
			flag_initialize(&fl);
			flag_search(format, &i, &fl);
			ll_hh(&fl);
			if (p_func1(format, &i, ap, &fl) == -1)
				return (-1);
		}
		else
		{
			if (ft_simple_print_char(format[i]) == -1)
				return (-1);
			i++;
		}
	va_end(ap);
	return (g_count);
}
