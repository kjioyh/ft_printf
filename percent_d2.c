/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pecent_d1.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 17:04:09 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/25 17:04:26 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	vd_width(t_f *fl, char *str, int *arr)
{
	int width;

	width = fl->width - max_of_two(arr[1], fl->precision);
	if (width > 0)
	{
		if (fl->plus == 1 || fl->space == 1 || arr[0] == 1)
			width--;
		if (fl->minus == 0)
		{
			while (width != 0)
			{
				if (fl->zero == 1 && fl->precision == -1)
					str[arr[4]] = '0';
				else
					str[arr[4]] = ' ';
				arr[4] += 1;
				width--;
			}
		}
		else
		{
			arr[2] = width;
		}
	}
}

void	vd_array_initialize(int *arr)
{
	int i;

	i = 0;
	while (i < 5)
	{
		arr[i] = 0;
		i++;
	}
}

void	vd_print_free(char **buf, char **ptr)
{
	vd_putstr(*buf);
	ft_strdel(buf);
	ft_strdel(ptr);
}

int		free_buffers(char **buf, char **ptr)
{
	ft_strdel(buf);
	ft_strdel(ptr);
	return (-1);
}

void	vd_zero_print(char *buf, int *arr)
{
	int flag;

	flag = 0;
	if (arr[2] != 0)
	{
		ft_putchar('\0');
		g_count += 1;
		flag = 1;
	}
	while (*buf != '\0')
	{
		ft_putchar(*buf);
		buf++;
		g_count += 1;
	}
	if (flag == 0)
	{
		ft_putchar('\0');
		arr[2] = 0;
		g_count += 1;
	}
}
