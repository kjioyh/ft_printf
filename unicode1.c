/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unicode.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/21 16:52:22 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/27 14:38:25 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			vd_bytes_count(unsigned int c)
{
	if (0 < c && c <= 127)
	{
		return (1);
	}
	else if (128 <= c && c <= 2047)
	{
		return (2);
	}
	else if (2048 <= c && c <= 65535)
	{
		return (3);
	}
	else if (65536 <= c && c <= 2097151)
	{
		return (4);
	}
	else
	{
		return (0);
	}
}

void		ft_unichar1(char *ptr, unsigned int n, int *j)
{
	int		len;
	char	temp;

	len = vd_bytes_count(n);
	if (len == 0)
	{
		ptr[*j] = '\0';
		*j += 1;
	}
	else if (len == 1)
	{
		temp = n;
		ptr[*j] = temp;
		*j += 1;
	}
	else if (len == 2)
	{
		temp = (n >> 6) | 192;
		ptr[*j] = temp;
		temp = (n & 63) | 128;
		ptr[*j + 1] = temp;
		*j += 2;
	}
	else
		ft_unichar2(ptr, n, j, len);
}

void		ft_unichar2(char *ptr, unsigned int n, int *j, int len)
{
	char	temp;

	if (len == 3)
	{
		temp = (n >> 12) | 224;
		ptr[*j] = temp;
		temp = ((n >> 6) & 63) | 128;
		ptr[*j + 1] = temp;
		temp = (n & 63) | 128;
		ptr[*j + 2] = temp;
		*j += 3;
	}
	else if (len == 4)
	{
		temp = (n >> 18) | 240;
		ptr[*j] = temp;
		temp = ((n >> 12) & 63) | 128;
		ptr[*j + 1] = temp;
		temp = ((n >> 6) & 63) | 128;
		ptr[*j + 2] = temp;
		temp = (n & 63) | 128;
		ptr[*j + 3] = temp;
		*j += 4;
	}
}
