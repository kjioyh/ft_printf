/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   percent_p.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/18 15:05:54 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/18 16:35:53 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	vd_memory_hash(int *arr, char *str)
{
	ft_strcpy(&str[arr[4]], "0x");
	arr[4] += 2;
	arr[3] = 1;
}

void	vd_memory_width(t_f *fl, char *str, int *arr)
{
	int width;

	width = fl->width - max_of_two(arr[1] + 2, fl->precision);
	if (fl->precision > arr[1] && fl->width > fl->precision)
		width -= 2;
	if (width > 0)
	{
		if (fl->minus == 0)
		{
			while (width != 0)
			{
				if (fl->zero == 1 && fl->precision == -1)
					str[arr[4]] = '0';
				else
					str[arr[4]] = ' ';
				arr[4] += 1;
				width--;
			}
		}
		else
		{
			arr[2] = width;
		}
	}
}

int		vd_memory_width_search(t_f *fl, int len)
{
	int width;

	width = 0;
	if (fl->width > fl->precision && fl->width > len + 2)
	{
		width = fl->width;
	}
	else if (fl->precision >= fl->width && fl->precision >= len)
	{
		width = fl->precision;
	}
	else
	{
		width = len;
	}
	return (width);
}
