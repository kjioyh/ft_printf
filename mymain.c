/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/16 13:29:03 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/27 17:43:14 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdio.h>
#include <wchar.h>
#include <string.h>
#include <locale.h>

/*printf("hash: %d\n", flags.hash);
printf("space: %d\n", flags.space);
printf("plus: %d\n", flags.plus);
printf("minus: %d\n", flags.minus);
printf("zero: %d\n", flags.zero);
printf("width: %d\n", flags.width);
printf("precision: %d\n", flags.precision);
printf("l: %d\n", flags.l);
printf("ll: %d\n", flags.ll);
printf("h: %d\n", flags.h);
printf("hh: %d\n", flags.hh);
printf("j: %d\n", flags.j);
printf("z: %d\n", flags.z);
printf("number of h: %d\n", flags.h_count);
printf("number of l: %d\n", flags.l_count);
fflush(stdout);*/

int		main(void)
{
	setlocale(LC_ALL, "");
	unsigned long huy = 18446744073709551615LU;
	char *str = "hello";
	int n;
	int m;
	int nbr = 2147483648;
	int longnbr = -123456;

	n = ft_printf("Mark is retarded; he says %c. He does it %d%% of the %s\n", 'A', 55, "time");
	m = printf("Mark is retarded; he says %c. He does it %d%% of the %s\n", 'A', 55, "time");
	printf("n is %d\n", n);
	printf("m is %d\n", m);
fflush(stdout);
	n = ft_printf("%d in octal is %o, in hex is %x or %X, and unsigned value is %u\n", -255, -255, -255, -255, -255);
	m = printf("%d in octal is %o, in hex is %x or %X, and unsigned value is %u\n", -255, -255, -255, -255, -255);
	printf("n is %d\n", n);
	printf("m is %d\n", m);
fflush(stdout);
/*	n = ft_printf("%p\n", str);
	m = printf("%p\n", str);
	printf("n is %d\n", n);
	printf("m is %d\n", m);
	fflush(stdout);   */
	/*n = printf("%C\n", 256);
	printf("n is %d\n", n);
	m = printf("%C\n", 256);
	printf("m is %d\n", m);*/
	//m = printf("123%d123%2147483644d\n", 1,2);
	//printf("m is %d\n", m);
		//printf("% 10.0c\n", 'c');
	//      %s
	/*m = ft_printf("%# +015.5s\n", str);
	 printf("%d\n", m);
	 n = printf("%# +015.5s\n", str);
	 printf("%d\n", n);*/
	 /*      %c
	 *m = ft_printf("%030%\n", 'h');
	 *printf("%d\n", m);
	 *n = printf("%030%\n", 'h');
	 *printf("%d\n", n);*/
//ALL FLAGS    %d
/*
	m = ft_printf("% +hhd\n", nbr);//nothing
	n = printf("% +hhd\n", nbr);
	printf("%d\n", m);
	printf("%d\n", n);
fflush(stdout);
	m = ft_printf("% 06.2d\n", nbr);//p < w && w > nbr
	n = printf("% 06.2d\n", nbr);
	printf("%d\n", m);
	printf("%d\n", n);
fflush(stdout);
	m = ft_printf("%0 6.12d\n", nbr);//p > w && w > nbr
	n = printf("%0 6.12d\n", nbr);
	printf("%d\n", m);
	printf("%d\n", n);
fflush(stdout);
	m = ft_printf("%0 1.7d\n", nbr); //p > w && w < nbr
	n = printf("%0 1.7d\n", nbr);
	printf("%d\n", m);
	printf("%d\n", n);
fflush(stdout);
	m = ft_printf("%0 4.2d\n", longnbr); //p < w && p < nbr
	n = printf("%0 4.2d\n", longnbr);
	printf("%d\n", m);
	printf("%d\n", n);
fflush(stdout);
	m = ft_printf("%0 2.4d\n", longnbr); //p > w && w < nbr
	n = printf("%0 2.4d\n", longnbr);
	printf("%d\n", m);
	printf("%d\n", n);
fflush(stdout);
	m = ft_printf("%0 010d\n", nbr);//NO p && w > nbr
	n = printf("%0 010d\n", nbr);
	printf("%d\n", m);
	printf("%d\n", n);
fflush(stdout);
	m = ft_printf("%0 03d\n", nbr);//NO p && w == nbr
	n = printf("%0 03d\n", nbr);
	printf("%d\n", m);
	printf("%d\n", n);
fflush(stdout);
	m = ft_printf("%0 02d\n", nbr);//NO p && w < nbr
	n = printf("%0 02d\n", nbr);
	printf("%d\n", m);
	printf("%d\n", n);
fflush(stdout);
	m = ft_printf("%0 .10d\n", nbr);//NO w && p > nbr
	n = printf("%0 .10d\n", nbr);
	printf("%d\n", m);
	printf("%d\n", n);
fflush(stdout);
	m = ft_printf("%0 .3d\n", nbr);//NO w && p == nbr
	n = printf("%0 .3d\n", nbr);
	printf("%d\n", m);
	printf("%d\n", n);
fflush(stdout);
	m = ft_printf("%0 .1d\n", nbr);//NO w && p < nbr
	n = printf("%0 .1d\n", nbr);
	printf("%d\n", m);
	printf("%d\n", n);
fflush(stdout);
	m = ft_printf("% 010.5d\n", nbr);//p > nbr && p < w
	n = printf("% 010.5d\n", nbr);
	printf("%d\n", m);
	printf("%d\n", n);
fflush(stdout);
	m = ft_printf("%0 3.3d\n", nbr);//p == w == nbr
	n = printf("%0 3.3d\n", nbr);
	printf("%d\n", m);
	printf("%d\n", n);
	fflush(stdout);*/
// %o
/*
	m = ft_printf("%ll-5.1o\n", nbr);
	n = printf("%ll-5.1o\n", nbr);
	printf("%d\n", m);
	printf("%d\n", n);
	fflush(stdout);
*/
// %x
/*
	m = ft_printf("%#5.4X\n", nbr);
	n = printf("%#5.4X\n", nbr);
	printf("%d\n", m);
	printf("%d\n", n);
	fflush(stdout);
*/
// %u
/*	m = ft_printf("%-10.2u\n", 0);
	n = printf("%-10.2u\n", 0);
	printf("%d\n", m);
	printf("%d\n", n);
	fflush(stdout);*/
//  %p
/*
	m = ft_printf("hello%30+0.15p\n", str);
	n = printf("hello%30+0.15p\n", str);
	printf("%d\n", m);
	printf("%d\n", n);
	fflush(stdout);
*/
//    %c

/*	m = ft_printf("%c\n", 145);
	n = printf("%c\n", 145);
	printf("%d\n", m);
	printf("%d\n", n);
	fflush(stdout);
*/

//     %NOTHING
/*	m = ft_printf("%.- z 7 yhgjk\n", 1);
	n = printf("%.- z 7 yhgjk\n", 1);
//n = printf("%5%d\n", 1);
	printf("%d\n", m);
	printf("%d\n", n);
	fflush(stdout);
*/
//    %s NULL
/*
	m = ft_printf("%012s\n", NULL);
	n = printf("%012s\n", NULL);
	printf("%d\n", m);
	printf("%d\n", n);
	fflush(stdout);*/
//     hh and ll tests
/*
	m = ft_printf("%h  hh # h15u\n", -1);
	n =    printf("%h  hh # h15u\n", -1);
	printf("%d\n", m);
	printf("%d\n", n);
	fflush(stdout);*/

//     %S
/*	wchar_t *S = L"ðண";
//	wchar_t *S = NULL;
	m = ft_printf("ft_printf:%S\n", S);
	   n = printf("stdprintf:%S\n", S);
	printf("m is %d\n", m);
	printf("n is %d\n", n);
	fflush(stdout);
*/
//	ft_printf("my:%lld\n", -9223372036854775809);
//	printf("no:%lld\n", -9223372036854775809);
//   zero char
/*m = ft_printf("%c", 0);
	printf("\n%d\n", m);
	n = printf("%c", 0);
	printf("\n%d\n", n);*/

	m = ft_printf("%kkkkk");
	n = printf("%kkkkk");
	printf("%d\n", m);
	printf("%d\n", n);
	fflush(stdout);
	return (0);
}
