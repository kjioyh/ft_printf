/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fork.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/17 18:18:55 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/27 19:15:11 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_id(void *ptr, t_f *fl, char c)
{
	if (c == 'D')
		fl->l = 1;
	if (fl->j == 1)
		return (ft_digit((intmax_t)ptr, fl));
	else if (fl->z == 1)
		return (ft_digit((size_t)ptr, fl));
	else if (fl->ll == 1)
		return (ft_digit((long long)ptr, fl));
	else if (fl->l == 1)
		return (ft_digit((long)ptr, fl));
	else if (fl->h == 1)
		return (ft_digit((short)ptr, fl));
	else if (fl->hh == 1)
		return (ft_digit((char)ptr, fl));
	return (ft_digit((int)ptr, fl));
}

int		ft_o(void *ptr, t_f *fl, int c)
{
	if (c == 'O')
		fl->l = 1;
	if (fl->j == 1)
		return (ft_octal((uintmax_t)ptr, fl));
	else if (fl->z == 1)
		return (ft_octal((size_t)ptr, fl));
	else if (fl->ll == 1)
		return (ft_octal((unsigned long long)ptr, fl));
	else if (fl->l == 1)
		return (ft_octal((unsigned long)ptr, fl));
	else if (fl->h == 1)
		return (ft_octal((unsigned short)ptr, fl));
	else if (fl->hh == 1)
		return (ft_octal((unsigned char)ptr, fl));
	return (ft_octal((unsigned int)ptr, fl));
}

int		ft_x(void *ptr, t_f *fl, char letter)
{
	if (fl->j == 1)
		return (ft_hex((uintmax_t)ptr, letter, fl));
	else if (fl->z == 1)
		return (ft_hex((size_t)ptr, letter, fl));
	else if (fl->ll == 1)
		return (ft_hex((unsigned long long)ptr, letter, fl));
	else if (fl->l == 1)
		return (ft_hex((unsigned long)ptr, letter, fl));
	else if (fl->h == 1)
		return (ft_hex((unsigned short)ptr, letter, fl));
	else if (fl->hh == 1)
		return (ft_hex((unsigned char)ptr, letter, fl));
	return (ft_hex((unsigned int)ptr, letter, fl));
}

int		ft_u(void *ptr, t_f *fl, int c)
{
	if (c == 'U')
		fl->l = 1;
	if (fl->j == 1)
		return (ft_unsigned((uintmax_t)ptr, fl));
	else if (fl->z == 1)
		return (ft_unsigned((size_t)ptr, fl));
	else if (fl->ll == 1)
		return (ft_unsigned((unsigned long long)ptr, fl));
	else if (fl->l == 1)
		return (ft_unsigned((unsigned long)ptr, fl));
	else if (fl->h == 1)
		return (ft_unsigned((unsigned short)ptr, fl));
	else if (fl->hh == 1)
		return (ft_unsigned((unsigned char)ptr, fl));
	return (ft_unsigned((unsigned int)ptr, fl));
}

int		ft_c(void *ptr, t_f *fl, int c)
{
	if (c == 'C')
		fl->l = 1;
	if (fl->l == 1)
		return (ft_char((int)ptr, fl));
	return (ft_char((int)ptr, fl));
}
