/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/10 12:30:16 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/27 12:12:49 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		ft_putchar(int c)
{
	write(1, &c, 1);
}

char		*ft_strnew(int size)
{
	char	*ptr;
	int		i;

	if (!(ptr = (char *)malloc(sizeof(char) * size + 1)))
	{
		return (ptr);
	}
	ptr[size] = '\0';
	i = 0;
	while (size != 0)
	{
		ptr[i] = '\0';
		size--;
		i++;
	}
	return (ptr);
}

char		*ft_strcpy(char *dst, const char *src)
{
	int		i;

	i = 0;
	while (src[i] != '\0')
	{
		dst[i] = src[i];
		i++;
	}
	dst[i] = '\0';
	return (dst);
}

void		ft_strdel(char **as)
{
	if (as != NULL)
	{
		free(*as);
		*as = NULL;
	}
}
