/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   percent_d.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 14:34:01 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/27 12:04:33 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	vd_sign(t_f *fl, int *arr, char *str, int sign)
{
	if (fl->plus == 0 && fl->space == 1)
	{
		if (sign == 0)
		{
			str[arr[4]] = ' ';
			arr[4] += 1;
		}
	}
	else if (fl->plus == 1)
	{
		if (sign == 0)
		{
			str[arr[4]] = '+';
			arr[4] += 1;
		}
	}
	if (sign == 1)
	{
		str[arr[4]] = '-';
		arr[4] += 1;
	}
}

int		vd_width_search(t_f *fl, int len)
{
	int	width;

	width = 0;
	if (fl->width > fl->precision && fl->width > len)
	{
		width = fl->width;
	}
	else if (fl->precision >= fl->width && fl->precision >= len)
	{
		width = fl->precision;
	}
	else
	{
		width = len;
	}
	return (width);
}

int		max_of_two(int a, int b)
{
	if (a < 0)
	{
		return (b);
	}
	else if (b < 0)
	{
		return (a);
	}
	if (a <= b)
	{
		return (b);
	}
	else
	{
		return (a);
	}
}

void	vd_precision(t_f *fl, int len, char *str, int *arr)
{
	int	width;

	if (fl->precision >= 0)
	{
		width = fl->precision - len;
		if (width > 0)
		{
			while (width != 0)
			{
				str[arr[4]] = '0';
				arr[4] += 1;
				width--;
			}
		}
	}
}

void	vd_minus(int minus_sign, char *str)
{
	int	len;

	len = vd_strlen(str);
	while (minus_sign != 0)
	{
		str[len] = ' ';
		len++;
		minus_sign--;
	}
}
