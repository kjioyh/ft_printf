/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vd_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 13:23:26 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/27 16:15:26 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static size_t	ptr_len(uintmax_t nbr, int base)
{
	int			len;

	len = 0;
	while ((nbr = nbr / base) != 0)
	{
		len++;
	}
	return (len);
}

static void		vd_oux(char *ptr, uintmax_t nbr, size_t len, int base)
{
	char		*hex_base;

	hex_base = "0123456789abcdef";
	while (nbr != 0)
	{
		ptr[len] = hex_base[nbr % base];
		nbr /= base;
		len--;
	}
}

static void		vd_str_toupper(char *ptr)
{
	while (*ptr != '\0')
	{
		if ('a' <= *ptr && *ptr <= 'z')
		{
			*ptr -= 32;
		}
		ptr++;
	}
}

char			*vd_huo_itoa(uintmax_t nbr, int base, int flag)
{
	char		*ptr;
	int			len;

	len = ptr_len(nbr, base);
	if (!(ptr = ft_strnew(len + 1)))
		return (NULL);
	if (nbr == 0)
	{
		ptr[0] = '0';
		return (ptr);
	}
	vd_oux(ptr, nbr, len, base);
	if (flag == 1)
	{
		vd_str_toupper(ptr);
	}
	return (ptr);
}
