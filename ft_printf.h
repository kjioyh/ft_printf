/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/24 17:57:36 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/28 16:35:26 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdlib.h>
# include <unistd.h>
# include <stdarg.h>

int					g_count;
typedef struct		s_fl
{
	int				hash;
	int				space;
	int				plus;
	int				minus;
	int				zero;
	int				width;
	int				precision;
	int				hh;
	int				h;
	int				h_count;
	int				l;
	int				ll;
	int				l_count;
	int				z;
	int				j;
}					t_f;

int					ft_printf(const char *format, ...);
int					p_func1(const char *format, int *i, va_list ap, t_f *fl);
int					p_func2(const char *format, int *i, va_list ap, t_f *fl);

int					ft_simple_print_char(int c);
void				ft_putchar(int c);
char				*vd_strncpy(char *dst, const char *src, int len);
char				*vd_strcpy(char *dst, const char *src, int *arr);
int					vd_strlen(char *str);
void				vd_putstr(char *str);
char				*ft_strnew(int size);
char				*ft_strcpy(char *dst, const char *src);
void				ft_strdel(char **as);

char				*vd_decimal_itoa(intmax_t nbr);
char				*vd_huo_itoa(uintmax_t nbr, int base, int flag);

int					ft_digit(intmax_t d, t_f *fl);
int					ft_char(int c, t_f *fl);
int					ft_string(char *str, t_f *fl);
int					ft_octal(uintmax_t octal, t_f *fl);
int					ft_unsigned(uintmax_t unsign, t_f *fl);
int					ft_memory(void *p, t_f *fl);
int					ft_hex(uintmax_t hex, char letter, t_f *fl);

void				flag_initialize(t_f *fl);
void				flag_search(const char *format, int *i, t_f *fl);
int					ft_isflag(char c);
void				ft_precision(const char *format, int *i, t_f *fl);
void				ft_width(const char *format, int *i, t_f *fl);
void				ft_flags(const char *format, int *i, t_f *fl);
void				ft_length_mod(const char *format, int *i, t_f *fl);
void				ll_hh(t_f *fl);

void				vd_sign(t_f *fl, int *i, char *str, int sign);
int					vd_width_search(t_f *fl, int len);
int					max_of_two(int a, int b);
void				vd_precision(t_f *fl, int len, char *str, int *arr);
void				vd_minus(int minus_sign, char *str);
void				vd_width(t_f *fl, char *str, int *arr);
void				vd_array_initialize(int *arr);
void				vd_print_free(char **buf, char **ptr);
void				vd_zero_print(char *buf, int *arr);
int					free_buffers(char **buf, char **ptr);
void				vd_start_check(int *arr, char *buf);

void				vd_char_width(t_f *fl, char *str, int *arr);
void				vd_string_width(t_f *fl, char *str, int *arr);

void				vd_octal_width(t_f *fl, char *str, int *arr);
void				vd_octal_hash(t_f *fl, int *arr, char *str);
void				vd_octal_precision(t_f *fl, int len, char *str, int *arr);

void				vd_hexa_width(t_f *fl, char *str, int *arr);
void				vd_hexa_hash(t_f *fl, int *arr, char *str, char letter);
char				*vd_hexa_convert(char letter, uintmax_t hex);
void				ft_zero_check(uintmax_t hex, t_f *fl, char *ptr, int *arr);

void				vd_unsign_width(t_f *fl, char *str, int *arr);

void				vd_memory_hash(int *arr, char *str);
void				vd_memory_width(t_f *fl, char *str, int *arr);
int					vd_memory_width_search(t_f *fl, int len);

int					ft_id(void *ptr, t_f *fl, char c);
int					ft_o(void *ptr, t_f *fl, int c);
int					ft_x(void *ptr, t_f *fl, char letter);
int					ft_u(void *ptr, t_f *fl, int c);
int					ft_c(void *ptr, t_f *fl, int c);

void				ft_unichar1(char *ptr, unsigned int n, int *j);
void				ft_unichar2(char *ptr, unsigned int n, int *j, int len);
int					vd_bytes_count(unsigned int c);
int					ft_unistring(wchar_t *str, t_f *fl);
char				*unicode_to_array(t_f *fl, wchar_t *str);
int					wsize_no_prec(int *wstr);
int					wsize_w_prec(int *wstr, int precision);
#endif
