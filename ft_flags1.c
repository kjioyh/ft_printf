/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_flags.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 14:36:45 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/28 17:53:50 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	flag_initialize(t_f *fl)
{
	fl->hash = 0;
	fl->space = 0;
	fl->plus = 0;
	fl->minus = 0;
	fl->zero = 0;
	fl->width = 0;
	fl->precision = -1;
	fl->hh = 0;
	fl->h = 0;
	fl->h_count = 0;
	fl->l = 0;
	fl->ll = 0;
	fl->l_count = 0;
	fl->z = 0;
	fl->j = 0;
}

void	ft_flags(const char *format, int *i, t_f *fl)
{
	if (format[*i] == '#' && fl->hash == 0)
	{
		fl->hash = 1;
	}
	else if (format[*i] == ' ' && fl->space == 0)
	{
		fl->space = 1;
	}
	else if (format[*i] == '+' && fl->plus == 0)
	{
		fl->plus = 1;
	}
	else if (format[*i] == '-' && fl->minus == 0)
	{
		fl->minus = 1;
	}
	else if (format[*i] == '0' && fl->zero == 0)
	{
		fl->zero = 1;
	}
	*i += 1;
}

void	flag_search(const char *format, int *i, t_f *fl)
{
	*i += 1;
	while (ft_isflag(format[*i]) == 1)
	{
		if (format[*i] == '#' || format[*i] == '+' || format[*i] == '-'
			|| format[*i] == ' ' || format[*i] == '0')
		{
			ft_flags(format, i, fl);
		}
		else if (format[*i] == '.')
		{
			ft_precision(format, i, fl);
		}
		else if ('1' <= format[*i] && format[*i] <= '9')
		{
			ft_width(format, i, fl);
		}
		else if (format[*i] == 'h' || format[*i] == 'l' || format[*i] == 'z'
				|| format[*i] == 'j')
		{
			ft_length_mod(format, i, fl);
		}
	}
}

void	ft_length_mod(const char *format, int *i, t_f *fl)
{
	if (format[*i] == 'j' && fl->j == 0)
	{
		fl->j = 1;
	}
	else if (format[*i] == 'z' && fl->z == 0)
	{
		fl->z = 1;
	}
	else if (format[*i] == 'h')
	{
		fl->h_count += 1;
	}
	else if (format[*i] == 'l')
	{
		fl->l_count += 1;
	}
	*i += 1;
}

void	ll_hh(t_f *fl)
{
	if (fl->h_count != 0)
	{
		if ((fl->h_count % 2) == 0)
		{
			fl->hh = 1;
		}
		else
		{
			fl->h = 1;
		}
	}
	if (fl->l_count != 0)
	{
		if ((fl->l_count % 2) == 0)
		{
			fl->ll = 1;
		}
		else
		{
			fl->l = 1;
		}
	}
}
