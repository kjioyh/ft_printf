/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unicode2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/27 14:39:56 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/27 18:05:47 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			wsize_no_prec(int *wstr)
{
	int		i;
	int		res;

	i = 0;
	res = 0;
	while (wstr[i] != 0)
	{
		res += vd_bytes_count(wstr[i]);
		i++;
	}
	return (res);
}

int			wsize_w_prec(int *wstr, int precision)
{
	int		i;
	int		res;
	int		temp;

	i = 0;
	res = 0;
	while (wstr[i] != 0)
	{
		temp = vd_bytes_count(wstr[i]);
		if (temp + res > precision)
		{
			break ;
		}
		res += temp;
		i++;
	}
	return (res);
}

char		*unicode_to_array(t_f *fl, wchar_t *str)
{
	int		len;
	int		i;
	int		j;
	char	*ptr;

	len = 0;
	if (fl->precision == -1)
	{
		len = wsize_no_prec((int *)str);
	}
	else if (fl->precision != -1)
	{
		len = wsize_w_prec((int *)str, fl->precision);
	}
	ptr = ft_strnew(len);
	j = 0;
	i = 0;
	while (j < len)
	{
		ft_unichar1(ptr, (unsigned int)str[i], &j);
		i++;
	}
	return (ptr);
}
