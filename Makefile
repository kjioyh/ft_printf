#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/02/27 18:02:28 by vdoroshy          #+#    #+#              #
#    Updated: 2017/02/27 18:03:36 by vdoroshy         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = libftprintf.a
CC = gcc
CFLAGS = -Wall -Wextra -Werror

OBJECTS = conversions1.o conversions2.o fork.o ft_flags1.o ft_flags2.o ft_printf.o libft1.o libft2.o percent_d1.o percent_d2.o percent_p.o percent_soc.o percent_xu.o unicode1.o unicode2.o vd_itoa.o vd_itoa_base.o 

.PHONY : clean all fclean re

all : $(NAME)

$(NAME) : $(OBJECTS)
	ar rc $(NAME) $(OBJECTS)
	ranlib $(NAME)

clean :
	rm -rf $(OBJECTS)

fclean : clean
	rm -rf $(NAME)

re : fclean all

conversions1.o : conversions1.c
	$(CC) $(CFLAGS) -c conversions1.c

conversions2.o : conversions2.c
	$(CC) $(CFLAGS) -c conversions2.c

fork.o : fork.c
	$(CC) $(CFLAGS) -c fork.c

ft_flags1.o : ft_flags1.c
	$(CC) $(CFLAGS) -c ft_flags1.c

ft_flags2.o : ft_flags2.c
	$(CC) $(CFLAGS) -c ft_flags2.c	

ft_printf.o : ft_printf.c
	$(CC) $(CFLAGS) -c ft_printf.c

libft1.o : libft1.c
	$(CC) $(CFLAGS) -c libft1.c

libft2.o : libft2.c
	$(CC) $(CFLAGS) -c libft2.c

percent_d1.o : percent_d1.c
	$(CC) $(CFLAGS) -c percent_d1.c

percent_d2.o : percent_d2.c
	$(CC) $(CFLAGS) -c percent_d2.c

percent_p.o : percent_p.c
	$(CC) $(CFLAGS) -c percent_p.c

percent_soc.o : percent_soc.c
	$(CC) $(CFLAGS) -c percent_soc.c

percent_xu.o : percent_xu.c
	$(CC) $(CFLAGS) -c percent_xu.c

unicode1.o : unicode1.c
	$(CC) $(CFLAGS) -c unicode1.c

unicode2.o : unicode2.c
	$(CC) $(CFLAGS) -c unicode2.c

vd_itoa.o : vd_itoa.c
	$(CC) $(CFLAGS) -c vd_itoa.c

vd_itoa_base.o : vd_itoa_base.c
	$(CC) $(CFLAGS) -c vd_itoa_base.c