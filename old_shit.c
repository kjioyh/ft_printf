void	ft_length_mod(const char *format, size_t *i, t_flags *flags)
{
    if (format[*i] == 'j' && flags->j == 0)
    {
        flags->j = 1;
    }
    else if (format[*i] == 'z' && flags->z == 0)
    {
        flags->z = 1;
    }
    else if (format[*i] == 'h' && format[*i + 1] == 'h')
    {
        flags->hh = 1;
        *i += 1;
    }
    else if (format[*i] == 'l' && format[*i + 1] == 'l')
    {
        flags->ll = 1;
        *i += 1;
    }
    else if (format[*i] == 'h' && format[*i + 1] != 'h' && flags->h == 0)
    {
        flags->h = 1;
    }
    else if (format[*i] == 'l' && format[*i + 1] != 'l' && flags->l == 0)
    {
        flags->l = 1;
    }
    *i += 1;
}





int       ft_digit(int d, t_flags *flags)   //TODO my algo, no minus
{                                                                                                                                  
    int sign;                                                                                                                      
    char *ptr;                                                                                                                     
    int len;                                                                                                                       
    int width;                                                                                                                     
    int count;                                                                                                                     
                                                                                                                                   
    sign = (d < 0) ? 1 : 0;                                                                                                        
    fflush(stdout);                                                                                                                
    ptr = vd_decimal_itoa(d);                                                                                                      
    len = ft_strlen(ptr);                                                                                                          
    if (flags->precision > len && flags->precision >= flags->width)                                                                
    {                                                                                                                              
        width = flags->precision - len;                                                                                            
    }                                                                                                                              
    else if (flags->width > len && flags->width > flags->precision)                                                                
    {                                                                                                                              
        width = flags->width - len;                                                                                                
    }                                                                                                                              
    else                                                                                                                           
    {                                                                                                                              
        width = 0;                                                                                                                 
    }                                                                                                                              
    count = 0;                                                                                                                     
    if (flags->precision == -1 && flags->width == 0)                                                                               
    {                                                                                                                              
        if (flags->plus == 0 && flags->space == 1)                                                                                 
        {                                                                                                                          
            if (sign == 0)                                                                                                         
            {                                                                                                                      
				vd_putchar(' ', &count);                                                                                           
            }                                                                                                                      
		}                                                                                                                          
		else if (flags->plus == 1)                                                                                                 
		{                                                                                                                          
            if (sign == 0)                                                                                                         
            {                                                                                                                      
				vd_putchar('+', &count);                                                                                           
            }                                                                                                                      
		}                                                                                                                          
		if (sign == 1)                                                                                                             
		{                                                                                                                          
       		vd_putchar('-', &count);                                                                                               
		}                                                                                                                          
	}
    else if (flags->precision != -1 && flags->width > flags->precision && flags->precision >= len)                                 
    {                                                                                                                              
		len = width - (flags->precision - len);                                                                                    
		width -= len;                                                                                                              
		if (flags->plus == 1 || flags->space == 1)                                                                                 
		{                                                                                                                          
		len -= 1;                                                                                                              
		}                                                                                                                          
	while (len != 0)                                                                                                           
	{                                                                                                                          
	vd_putchar(' ', &count);                                                                                               
	len--;                                                                                                                 
	}                                                                                                                          
	if (flags->plus == 0 && flags->space == 1)                                                                                 
	{                                                                                                                          
	if (sign == 0)                                                                                                         
	{                                                                                                                      
	vd_putchar(' ', &count);                                                                                           
	}                                                                                                                      
	}                                                                                                                          
	else if (flags->plus == 1)                                                                                                 
	{                                                                                                                          
	if (sign == 0)                                                                                                         
	{                                                                                                                      
	vd_putchar('+', &count);                                                                                           
	}                                                                                                                      
	}                                                                                                                          
	if (sign == 1)                                                                                                             
	{                                                                                                                          
	vd_putchar('-', &count);                                                                                               
	}                                                                                                                          
	while (width != 0)                                                                                                         
	{                                                                                                                          
	vd_putchar('0', &count);                                                                                               
	width--;                                                                                                               
	}                                                                                                                          
    }                                                                                                                                          
    else if (flags->precision == -1 && flags->width > len)                                                                         
    {                                                                                                                              
	if (flags->plus == 1 && flags->zero == 1)                                                                                  
	{                                                                                                                          
	if (sign == 1)                                                                                                         
	{                                                                                                                      
	vd_putchar('-', &count);                                                                                           
	}                                                                                                                      
	else                                                                                                                   
	{                                                                                                                      
	vd_putchar('+', &count);                                                                                           
	}                                                                                                                      
	width--;                                                                                                               
	}                                                                                                                          
	else if (flags->zero == 1)                                                                                                 
	{                                                                                                                          
	if (sign == 1)                                                                                                         
	{                                                                                                                      
	vd_putchar('-', &count);                                                                                           
	}                                                                                                                      
	else                                                                                                                   
	{                                                                                                                      
	vd_putchar(' ', &count);                                                                                           
	}                                                                                                                      
	width--;                                                                                                               
	}                                                                                                                          
	else if (flags->zero == 0)                                                                                                 
	{                                                                                                                          
	width--;                                                                                                               
	}                                                                                                                          
	while (width != 0)                                                                                                         
	{                                                                                                                          
	if (flags->zero == 1)                                                                                                  
	{                                                                                                                      
	vd_putchar('0', &count);                                                                                           
	}                                                                                                                      
	else                                                                                                                   
	{                                                                                                                      
	vd_putchar(' ', &count);                                                                                           
	}                                                                                                                      
	width--;                                                                                                               
	}                                                                                                                          
	if (flags->plus == 0 && flags->space == 1 && flags->zero == 0)                                                             
	{                                                                                                                          
	if (sign == 0)                                                                                                         
	{                                                                                                                      
	vd_putchar(' ', &count);                                                                                           
	}                                                                                                                      
	}                                                                                                                          
	else if (flags->plus == 1 && flags->zero == 0)                                                                             
	{                                                                                                                          
	if (sign == 0)                                                                                                         
	{                                                                                                                      
	vd_putchar('+', &count);                                                                                           
	}                                                                                                                      
	}                                                                                                                          
	if (sign == 1 && flags->zero == 0)                                                                                         
	{                                                                                                                          
	vd_putchar('-', &count);                                                                                               
	}                                                                                                                          
    }                           
    else if (flags->precision != -1 && flags->width > flags->precision && flags->precision < len)                                  
    {                                                                                                                              
	if ((flags->plus == 1 || flags->space == 1) && flags->width > len)                                                         
	{                                                                                                                          
	width--;                                                                                                               
	}                                                                                                                          
	while (width != 0)                                                                                                         
	{                                                                                                                          
	vd_putchar(' ', &count);                                                                                               
	width--;                                                                                                               
	}                                                                                                                          
	if (flags->plus == 0 && flags->space == 1)                                                                                 
	{                                                                                                                          
	if (sign == 0)                                                                                                         
	{                                                                                                                      
	vd_putchar(' ', &count);                                                                                           
	}                                                                                                                      
	}                                                                                                                          
	else if (flags->plus == 1)                                                                                                 
	{                                                                                                                          
	if (sign == 0)                                                                                                         
	{                                                                                                                      
	vd_putchar('+', &count);                                                                                           
	}                                                                                                                      
	}                                                                                                                          
	if (sign == 1)                                                                                                             
	{                                                                                                                          
	vd_putchar('-', &count);                                                                                               
	}                                                                                                                          
    }                                                                                                                              
    vd_putstr(ptr, &count);                                                                                                        
    free(ptr);                                                                                                                     
    return (count);                                                                                                                
}                                                                                                                                  

