/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_flags2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/28 17:50:15 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/28 18:48:18 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_isflag(char c)
{
	if (c == '#' || c == '0' || c == '+' || c == '-' || c == ' '
	|| ('1' <= c && c <= '9') || c == '.' || c == 'h' || c == 'l'
		|| c == 'z' || c == 'j')
	{
		return (1);
	}
	return (0);
}

void	ft_precision(const char *format, int *i, t_f *fl)
{
	int j;

	*i += 1;
	j = 0;
	while ('0' <= format[*i] && format[*i] <= '9')
	{
		j = j * 10 + format[*i] - 48;
		*i += 1;
	}
	fl->precision = j;
}

void	ft_width(const char *format, int *i, t_f *fl)
{
	int j;

	j = 0;
	while ('0' <= format[*i] && format[*i] <= '9')
	{
		j = j * 10 + format[*i] - 48;
		*i += 1;
	}
	fl->width = j;
}

void	vd_start_check(int *arr, char *buf)
{
	if (buf[0] == ' ' || buf[0] == '+' || buf[0] == '-')
	{
		arr[3] = 1;
	}
}

void	ft_zero_check(uintmax_t hex, t_f *fl, char *ptr, int *arr)
{
	if (hex == 0 && fl->precision == 0)
	{
		ptr[0] = '\0';
		arr[3] = 2;
	}
	else if (hex == 0)
	{
		arr[3] = 2;
	}
}
