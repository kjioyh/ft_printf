/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conversions2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/27 16:10:27 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/28 18:47:22 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			ft_memory(void *p, t_f *fl)
{
	char	*ptr;
	char	*buf;
	int		arr[5];
	long	i;

	i = (long)p;
	ptr = vd_huo_itoa(i, 16, 2);
	vd_array_initialize(arr);
	if (i == 0 && fl->precision == 0)
		ptr[0] = '\0';
	arr[1] = vd_strlen(ptr);
	buf = ft_strnew(vd_memory_width_search(fl, arr[1]) + 2);
	if ((fl->width > arr[1] && fl->zero == 1 && fl->precision == -1)
		|| (fl->precision >= arr[1] && fl->precision > fl->width))
		vd_memory_hash(arr, buf);
	vd_memory_width(fl, buf, arr);
	if (arr[3] != 1)
		vd_memory_hash(arr, buf);
	vd_precision(fl, arr[1], buf, arr);
	ft_strcpy(&buf[arr[4]], ptr);
	vd_minus(arr[2], buf);
	if ((g_count + vd_strlen(buf)) < 0)
		return (free_buffers(&buf, &ptr));
	vd_print_free(&buf, &ptr);
	return (1);
}

int			ft_unistring(wchar_t *str, t_f *fl)
{
	char	*buf;
	int		arr[5];
	char	*ptr;

	if (str == NULL)
	{
		return (ft_string("(null)", fl));
	}
	vd_array_initialize(arr);
	ptr = unicode_to_array(fl, str);
	arr[1] = vd_strlen(ptr);
	buf = ft_strnew(max_of_two(fl->width, arr[1]));
	vd_string_width(fl, buf, arr);
	ft_strcpy(&buf[arr[4]], ptr);
	vd_minus(arr[2], buf);
	if ((g_count + vd_strlen(buf)) < 0)
	{
		return (free_buffers(&buf, &ptr));
	}
	vd_print_free(&buf, &ptr);
	return (1);
}

int			ft_char(int c, t_f *fl)
{
	char	*buf;
	int		arr[5];

	vd_array_initialize(arr);
	arr[1] = (fl->width > 1) ? fl->width : 1;
	buf = ft_strnew(arr[1]);
	vd_char_width(fl, buf, arr);
	buf[arr[4]] = c;
	arr[4] += 1;
	vd_minus(arr[2], buf);
	if ((g_count + vd_strlen(buf)) < 0)
	{
		return (free_buffers(&buf, NULL));
	}
	if (c == 0)
	{
		vd_zero_print(buf, arr);
		ft_strdel(&buf);
	}
	else
	{
		vd_print_free(&buf, NULL);
	}
	return (1);
}
