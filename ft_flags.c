/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_flags.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 14:36:45 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/27 12:14:59 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	flag_initialize(t_flags *flags)
{
	flags->hash = 0;
	flags->space = 0;
	flags->plus = 0;
	flags->minus = 0;
	flags->zero = 0;
	flags->width = 0;
	flags->precision = -1;
	flags->hh = 0;
	flags->h = 0;
	flags->h_count = 0;
	flags->l = 0;
	flags->ll = 0;
	flags->l_count = 0;
	flags->z = 0;
	flags->j = 0;
}

void	ft_flags(const char *format, int *i, t_flags *flags)
{
	if (format[*i] == '#' && flags->hash == 0)
	{
		flags->hash = 1;
	}
	else if (format[*i] == ' ' && flags->space == 0)
	{
		flags->space = 1;
	}
	else if (format[*i] == '+' && flags->plus == 0)
	{
		flags->plus = 1;
	}
	else if (format[*i] == '-' && flags->minus == 0)
	{
		flags->minus = 1;
	}
	else if (format[*i] == '0' && flags->zero == 0)
	{
		flags->zero = 1;
	}
	*i += 1;
}

void	flag_search(const char *format, int *i, t_flags *flags)
{
	*i += 1;
	while (ft_isflag(format[*i]) == 1)
	{
		if (format[*i] == '#' || format[*i] == '+' || format[*i] == '-'
			|| format[*i] == ' ' || format[*i] == '0')
		{
			ft_flags(format, i, flags);
		}
		else if (format[*i] == '.')
		{
			ft_precision(format, i, flags);
		}
		else if ('1' <= format[*i] && format[*i] <= '9')
		{
			ft_width(format, i, flags);
		}
		else if (format[*i] == 'h' || format[*i] == 'l' || format[*i] == 'z'
				 || format[*i] == 'j')
		{
			ft_length_mod(format, i, flags);
		}
	}
}

void	ft_length_mod(const char *format, int *i, t_flags *flags)
{
	if (format[*i] == 'j' && flags->j == 0)
	{
		flags->j = 1;
	}
	else if (format[*i] == 'z' && flags->z == 0)
	{
		flags->z = 1;
	}
	else if (format[*i] == 'h')
	{
		flags->h_count += 1;
	}
	else if (format[*i] == 'l')
	{
		flags->l_count += 1;
	}
	*i += 1;
}

void	ll_hh(t_flags *flags)
{
	if (flags->h_count != 0)
	{
		if ((flags->h_count % 2) == 0)
		{
			flags->hh = 1;
		}
		else
		{
			flags->h = 1;
		}
	}
	if (flags->l_count != 0)
	{
		if ((flags->l_count % 2) == 0)
		{
			flags->ll = 1;
		}
		else
		{
			flags->l = 1;
		}
	}
}
/*
** if (ft_strchr("123456789#+-. ", c)
*/

int		ft_isflag(char c)
{
	if (c == '#' || c == '0' || c == '+' || c == '-' || c == ' '
	|| ('1' <= c && c <= '9') || c == '.' || c == 'h' || c == 'l'
		|| c == 'z' || c == 'j')
	{
		return (1);
	}
	return (0);
}

void	ft_precision(const char *format, int *i, t_flags *flags)
{
	int j;

	*i += 1;
	j = 0;
	while ('0' <= format[*i] && format[*i] <= '9')
	{
		j = j * 10 + format[*i] - 48;
		*i += 1;
	}
	flags->precision = j;
}

void	ft_width(const char *format, int *i, t_flags *flags)
{
	int j;

	j = 0;
	while ('0' <= format[*i] && format[*i] <= '9')
	{
		j = j * 10 + format[*i] - 48;
		*i += 1;
	}
	flags->width = j;
}
