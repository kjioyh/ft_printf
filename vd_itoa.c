/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vd_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 17:09:41 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/27 16:14:59 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	nbr_absolute(int nbr)
{
	if (nbr >= 0)
	{
		return (nbr);
	}
	else
	{
		return (-nbr);
	}
}

static int	nbr_len(intmax_t nbr)
{
	int		i;

	i = 0;
	while ((nbr = nbr / 10))
	{
		i++;
	}
	return (i + 1);
}

char		*vd_decimal_itoa(intmax_t nbr)
{
	int		len;
	char	*ptr;

	len = nbr_len(nbr);
	if (!(ptr = ft_strnew(len)))
		return (NULL);
	len--;
	if (nbr == 0)
	{
		ptr[0] = '0';
		return (ptr);
	}
	while (nbr != 0)
	{
		ptr[len] = nbr_absolute((nbr % 10)) + 48;
		nbr /= 10;
		len--;
	}
	return (ptr);
}
