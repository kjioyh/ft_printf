#include <stdio.h>

int		ft_power(int a, int b)
{
	int res;

	res = 1;
	while (b != 0)
	{
		res *= a;
		b--;
	}
	return (res);
}

int		vd_convert(int n)
{
	int res;
	int i;
	int remainder;

	res = 0;
	i = 0;
	while (n != 0)
	{
		remainder = n % 10;
		n /= 10;
		res += remainder * ft_power(2, i);
		i++;
	}
	return (res);
}
