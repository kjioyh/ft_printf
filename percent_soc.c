/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   percent_s.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/10 15:24:04 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/18 12:56:11 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	vd_string_width(t_f *fl, char *str, int *arr)
{
	int width;

	width = fl->width - arr[1];
	if (width > 0)
	{
		if (fl->minus == 0)
		{
			while (width != 0)
			{
				if (fl->zero == 1)
					str[arr[4]] = '0';
				else
					str[arr[4]] = ' ';
				arr[4] += 1;
				width--;
			}
		}
		else
		{
			arr[2] = width;
		}
	}
}

void	vd_octal_width(t_f *fl, char *str, int *arr)
{
	int width;

	width = fl->width - max_of_two(arr[1], fl->precision);
	if (width > 0)
	{
		if (fl->hash == 1 && fl->precision <= arr[1])
			width--;
		if (fl->minus == 0)
		{
			while (width != 0)
			{
				if (fl->zero == 1 && fl->precision == -1)
					str[arr[4]] = '0';
				else
					str[arr[4]] = ' ';
				arr[4] += 1;
				width--;
			}
		}
		else
		{
			arr[2] = width;
		}
	}
}

void	vd_octal_hash(t_f *fl, int *arr, char *str)
{
	if (fl->hash == 1)
	{
		str[arr[4]] = '0';
		arr[4] += 1;
	}
}

void	vd_octal_precision(t_f *fl, int len, char *str, int *arr)
{
	int width;

	if (fl->precision >= 0)
	{
		width = fl->precision - len;
		if (fl->hash == 1)
		{
			width -= 1;
		}
		if (width > 0)
		{
			while (width != 0)
			{
				str[arr[4]] = '0';
				arr[4] += 1;
				width--;
			}
		}
	}
}

void	vd_char_width(t_f *fl, char *str, int *arr)
{
	int width;

	width = fl->width - 1;
	if (width > 0)
	{
		if (fl->minus == 0)
		{
			while (width != 0)
			{
				if (fl->zero == 1)
					str[arr[4]] = '0';
				else
					str[arr[4]] = ' ';
				arr[4] += 1;
				width--;
			}
		}
		else
		{
			arr[2] = width;
		}
	}
}
