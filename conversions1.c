/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conversions1.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/27 15:43:34 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/28 18:46:55 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			ft_digit(intmax_t d, t_f *fl)
{
	char	*ptr;
	char	*buf;
	int		arr[5];

	vd_array_initialize(arr);
	arr[0] = (d < 0) ? 1 : 0;
	ptr = vd_decimal_itoa(d);
	if (d == 0 && fl->precision == 0)
		ptr[0] = '\0';
	arr[1] = vd_strlen(ptr);
	buf = ft_strnew(vd_width_search(fl, arr[1]) + 1);
	if ((fl->precision >= fl->width && fl->precision >= arr[1])
		|| (fl->zero == 1 && fl->precision == -1 && fl->width > arr[1]))
		vd_sign(fl, arr, buf, arr[0]);
	vd_start_check(arr, buf);
	vd_width(fl, buf, arr);
	if (arr[3] == 0)
		vd_sign(fl, arr, buf, arr[0]);
	vd_precision(fl, arr[1], buf, arr);
	ft_strcpy(&buf[arr[4]], ptr);
	vd_minus(arr[2], buf);
	if ((g_count + vd_strlen(buf)) < 0)
		return (free_buffers(&buf, &ptr));
	vd_print_free(&buf, &ptr);
	return (1);
}

int			ft_string(char *str, t_f *fl)
{
	char	*buf;
	int		arr[5];

	if (str == NULL)
		return (ft_string("(null)", fl));
	vd_array_initialize(arr);
	if (fl->precision != -1 && fl->precision < vd_strlen(str))
	{
		arr[1] = fl->precision;
	}
	else
	{
		arr[1] = vd_strlen(str);
	}
	buf = ft_strnew(max_of_two(fl->width, arr[1]));
	vd_string_width(fl, buf, arr);
	vd_strncpy(&buf[arr[4]], str, arr[1]);
	vd_minus(arr[2], buf);
	if ((g_count + vd_strlen(buf)) < 0)
	{
		return (free_buffers(&buf, NULL));
	}
	vd_print_free(&buf, NULL);
	return (1);
}

int			ft_octal(uintmax_t octal, t_f *fl)
{
	char	*ptr;
	char	*buf;
	int		arr[5];

	ptr = vd_huo_itoa(octal, 8, 0);
	vd_array_initialize(arr);
	if ((octal == 0 && fl->precision == 0) || (octal == 0 && fl->hash == 1))
	{
		ptr[0] = '\0';
	}
	arr[1] = vd_strlen(ptr);
	buf = ft_strnew(vd_width_search(fl, arr[1]) + 1);
	vd_octal_width(fl, buf, arr);
	vd_octal_hash(fl, arr, buf);
	vd_octal_precision(fl, arr[1], buf, arr);
	ft_strcpy(&buf[arr[4]], ptr);
	vd_minus(arr[2], buf);
	if ((g_count + vd_strlen(buf)) < 0)
	{
		return (free_buffers(&buf, &ptr));
	}
	vd_print_free(&buf, &ptr);
	return (1);
}

int			ft_unsigned(uintmax_t unsign, t_f *fl)
{
	char	*ptr;
	char	*buf;
	int		arr[5];

	ptr = vd_huo_itoa(unsign, 10, 3);
	vd_array_initialize(arr);
	if (unsign == 0 && fl->precision == 0)
	{
		ptr[0] = '\0';
	}
	arr[1] = vd_strlen(ptr);
	buf = ft_strnew(vd_width_search(fl, arr[1]) + 1);
	vd_unsign_width(fl, buf, arr);
	vd_precision(fl, arr[1], buf, arr);
	ft_strcpy(&buf[arr[4]], ptr);
	vd_minus(arr[2], buf);
	if ((g_count + vd_strlen(buf)) < 0)
	{
		return (free_buffers(&buf, &ptr));
	}
	vd_print_free(&buf, &ptr);
	return (1);
}

int			ft_hex(uintmax_t hex, char letter, t_f *fl)
{
	char	*ptr;
	char	*buf;
	int		arr[5];

	ptr = vd_hexa_convert(letter, hex);
	vd_array_initialize(arr);
	ft_zero_check(hex, fl, ptr, arr);
	arr[1] = vd_strlen(ptr);
	buf = ft_strnew(vd_width_search(fl, arr[1]) + 2);
	if (fl->zero == 1 && fl->precision == -1 && fl->width > arr[1])
		vd_hexa_hash(fl, arr, buf, letter);
	vd_hexa_width(fl, buf, arr);
	if (arr[3] == 0)
		vd_hexa_hash(fl, arr, buf, letter);
	vd_precision(fl, arr[1], buf, arr);
	ft_strcpy(&buf[arr[4]], ptr);
	vd_minus(arr[2], buf);
	if ((g_count + vd_strlen(buf)) < 0)
		return (free_buffers(&buf, &ptr));
	vd_print_free(&buf, &ptr);
	return (1);
}
