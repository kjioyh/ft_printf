/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft1.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vdoroshy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 15:17:37 by vdoroshy          #+#    #+#             */
/*   Updated: 2017/02/27 17:19:15 by vdoroshy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		vd_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		i++;
	}
	return (i);
}

void	vd_putstr(char *str)
{
	while (*str != '\0')
	{
		ft_putchar(*str);
		str++;
		g_count += 1;
	}
}

char	*vd_strncpy(char *dst, const char *src, int len)
{
	int i;

	i = 0;
	while (i < len)
	{
		dst[i] = src[i];
		i++;
	}
	return (dst);
}

char	*vd_strcpy(char *dst, const char *src, int *arr)
{
	int i;

	i = 0;
	while (src[i] != '\0')
	{
		dst[arr[4]] = src[i];
		arr[4] += 1;
		i++;
	}
	return (dst);
}

int		ft_simple_print_char(int c)
{
	if (0 <= g_count + 1 && g_count + 1 <= 2147483647)
	{
		ft_putchar(c);
		g_count += 1;
		return (1);
	}
	else
	{
		return (-1);
	}
}
